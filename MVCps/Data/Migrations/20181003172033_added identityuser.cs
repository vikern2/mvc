﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MVCps.Data.Migrations
{
    public partial class addedidentityuser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ownerId",
                table: "Products",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_ownerId",
                table: "Products",
                column: "ownerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_AspNetUsers_ownerId",
                table: "Products",
                column: "ownerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_AspNetUsers_ownerId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_ownerId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "ownerId",
                table: "Products");
        }
    }
}
