﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVCps.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public virtual List<Product> products { get; set; }
    }
}
