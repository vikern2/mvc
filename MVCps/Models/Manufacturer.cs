﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVCps.Models
{
    public class Manufacturer
    {
        public int ManufacturerId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string address { get; set; }
        public virtual List<Product> products { get; set; }
    }
}
