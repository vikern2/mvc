﻿using MVCps.Views.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace MVCps.Models
{
    public interface IProductRepository
    {
        IEnumerable<Product> getall();
        Task Save(Product product, ClaimsPrincipal principal);
        ProductEditViewModel getProductEditViewModel();
        void delete(int? id);
        void update(Product product);
        Product GetProduct(int? id);
        void SaveWithoutUser(Product product);
        Product GetProductApi(int? id);
    }
}
