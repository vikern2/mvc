﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVCps.Models
{
    public class About
    {
        public string DescriptionText{ get; set; }
        public string Name { get; set; }
    }
}
