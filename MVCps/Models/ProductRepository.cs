﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MVCps.Data;
using MVCps.Views.ViewModels;

namespace MVCps.Models
{
    public class ProductRepository : IProductRepository
    {
        private ApplicationDbContext db;
        private UserManager<IdentityUser> manager;
        public ProductRepository(UserManager<IdentityUser> manager, ApplicationDbContext db)
        {
            this.manager = manager;
            this.db = db;
        }

        public void delete(int? id)
        {
            db.Products.Remove(db.Products.Find(id));
            db.SaveChanges();
        }

        public IEnumerable<Product> getall()
        {
            IEnumerable<Product> products = db.Products;
            return db.Products;
        }

        public Product GetProduct(int? id)
        {
            return db.Products.Include(x => x.owner).Include(x => x.Manufacturer).Include(x => x.Category).Where(product => product.ProductId == id).First();
        }

        public Product GetProductApi(int? id)
        {
            return db.Products.Find(id);
        }

        public ProductEditViewModel getProductEditViewModel()
        {
            var pevm = new ProductEditViewModel();
            pevm.Categories = db.Categories.ToList<Category>();
            pevm.Manufacturers = db.Manufacturers.ToList<Manufacturer>();

            return pevm;
        }

        public async Task Save(Product product, ClaimsPrincipal principal)
        {
            var current = await manager.GetUserAsync(principal);
            product.owner = current;
            db.Products.Add(product);
            db.SaveChanges();
        }

        public void SaveWithoutUser(Product product)
        {
            product.owner = user;
            db.Products.Add(product);
            db.SaveChanges();
        }

       

        public void update(Product product)
        {
            db.Products.Update(product);
            db.SaveChanges();
        }
    }
}
