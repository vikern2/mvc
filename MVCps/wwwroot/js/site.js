﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$(document).ready(function () {
    var postList = $("#list");

    $.ajax({
        dataType: "json",
        url: "https://localhost:44380/api/products/",
        data: [],
        success: function (response) {
            console.log(response);
            postList.empty();

            for (let i = 0; i < response.length; i++) {
                let product = response[i];
                postList.append("<li>" + product["productId"] + " : " + product["name"] + "</li>");
            }
            postList.append("<li>Response: <pre>" + JSON.stringify(response) + "</pre></li>")
        }
    })
});