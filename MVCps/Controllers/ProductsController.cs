﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MVCps.Models;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MVCps.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private IProductRepository repository;
        public ProductsController(IProductRepository repository)
        {
            this.repository = repository;
        }

        // GET: api/Products
        [HttpGet]
        public IEnumerable<Product> Get()
        {
            IEnumerable<Product> products = repository.getall();
            return products;
        }

        // GET: api/Products/5
        [HttpGet("{id}")]
        public Product Get([FromRoute]int id)
        {
            return repository.GetProductApi(id);
        }

        // POST: api/Products
        [HttpPost]
        public void Post([FromBody] Product product)
        {
            repository.SaveWithoutUser(product);
        }

        // PUT: api/Products/5
        [HttpPut("{id}")]
        public void Put([FromRoute]int id, [FromBody] Product product)
        {
            product.ProductId = id;
            repository.update(product);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            repository.delete(id);
        }
    }
}
