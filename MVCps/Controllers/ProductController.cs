﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using MVCps.Models;
using MVCps.Views.ViewModels;

namespace MVCps.Controllers
{
    public class ProductController : Controller
    {

        private IProductRepository repository;
        public ProductController(IProductRepository repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            return View(repository.getall());
        }

        [Authorize]
        // GET: Product/Create
        public ActionResult Create()
        {
            var product = repository.getProductEditViewModel();
            return View(product);
        }

        [Authorize]
        // POST: Product/Create
        [HttpPost]
        public ActionResult Create([Bind("ProductId,Name,Description,Price,ManufacturerId,CategoryId")]ProductEditViewModel product)
        {
            ClaimsPrincipal user = this.User;

            var p = new Product {
                Name = product.Name,
                Description = product.Description,
                Price = product.Price,
                ManufacturerId = product.ManufacturerId,
                CategoryId = product.CategoryId };
            if (ModelState.IsValid)
            {
                try
                {
                    repository.Save(p, user).Wait();
                    TempData["message"] = string.Format("{0} har blitt opprettet", product.Name);
                    return RedirectToAction("Index");
                }
                catch
                {
                    return View();
                }
            }
            else
            {
                return View();
            }
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = repository.GetProduct(id);
            ViewData["CategoryId"] = product.CategoryId;
            ViewData["ManufacturerId"] = product.ManufacturerId;
            ViewData["OwnerId"] = product.owner.Id;


            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }

        public async Task<IActionResult> Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = repository.GetProduct(id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Product product)
        {
            if (id != product.ProductId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
               repository.update(product);
               
               return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        public async Task<IActionResult> Delete(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = repository.GetProduct(id);
            ViewData["OwnerId"] = product.owner.Id;
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            repository.delete(id);
            return RedirectToAction("Index");
        }

        public ActionResult jquery()
        {
            return View();
        }


    }


}