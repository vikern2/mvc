﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MVCps.Models;

namespace MVCps.Controllers
{
    public class AboutController : Controller
    {
        About a = new About {DescriptionText="Denne siden er laget av", Name ="Erik"};
        public ActionResult About()
        {
            return View(a);
        }
    }
}