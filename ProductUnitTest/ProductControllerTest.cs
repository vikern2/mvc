using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVCps.Controllers;
using MVCps;
using MVCps.Models;
using System.Collections;
using System.Collections.Generic;
using Moq;

namespace ProductUnitTest
{
    [TestClass]
    public class ProductControllerTest
    {
        [TestMethod]
        public void IndexReturnsNotNullResult()
        {
            // Arrange
            var controller = new ProductController(new Mock<IProductRepository>().Object);
            // Act
            var result = (ViewResult)controller.Index();
            // Assert
            Assert.IsNotNull(result, "View Result is null");
        }

        Mock<IProductRepository> _repository;
        [TestMethod]
        public void IndexReturnsAllProducts()
        {
            // Arrange
            _repository = new Mock<IProductRepository>();
            List<Product> fakeproducts = new List<Product>{
                 new Product {Name="a", Price=121.50m, Description="Verkt�y"},
                 new Product {Name="Vinkelsliper", Price=1520.00m, Description="Verkt�y"},
                 new Product {Name="Melk", Price=14.50m, Description="Dagligvarer"},
                 new Product {Name="Kj�ttkaker", Price=32.00m, Description="Dagligvarer"},
                 new Product {Name="Br�d", Price=25.50m, Description="Dagligvarer"}
            };
            _repository.Setup(x => x.getall()).Returns(fakeproducts);
            var controller = new ProductController(_repository.Object);
            // Act
            var result = (ViewResult)controller.Index();
            // Assert
            CollectionAssert.AllItemsAreInstancesOfType((ICollection)result.ViewData.Model,
           typeof(Product));
            Assert.IsNotNull(result, "View Result is null");
            var products = result.ViewData.Model as List<Product>;
            Assert.AreEqual(5, products.Count, "Got wrong number of products");
        }

    }
}
